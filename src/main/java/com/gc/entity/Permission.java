package com.gc.entity;

/**
 * Created by Administrator on 2018/1/19 0019.
 */
public class Permission {
    private Integer id;
    private String pername;
    private String identification;
    private String mname;

    public Integer getModuleid() {
        return moduleid;
    }

    public void setModuleid(Integer moduleid) {
        this.moduleid = moduleid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    private Integer moduleid;

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public Permission() {
    }

    public Permission(Integer id, String pername) {
        this.id = id;
        this.pername = pername;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPername() {
        return pername;
    }

    public void setPername(String pername) {
        this.pername = pername;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "id=" + id +
                ", pername='" + pername + '\'' +
                '}';
    }
}
