package com.gc.common;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Created by Administrator on 2018/2/7 0007.
 */
@Aspect
@Component
public class LogAop {
    Logger log = Logger.getLogger(LogAop.class);

    @Pointcut("execution(* com.gc.service.impl.*.*(..))")
    public void pc(){}

    @Around("pc()")
    public Object around(ProceedingJoinPoint joinPoint){
        Object result = null;
        try {
            log.info("调用【"+joinPoint.getTarget().getClass().getSimpleName()+"】类的【"+joinPoint.getSignature()+"】方法传入参数为："+ Arrays.toString(joinPoint.getArgs()));
            result = joinPoint.proceed();
            log.info("【"+joinPoint.getSignature()+"】方法返回的结果为："+result);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            log.error("【"+joinPoint.getSignature()+"】方法出现异常，异常信息为："+throwable.getMessage());
        }
        return result;
    }
}
