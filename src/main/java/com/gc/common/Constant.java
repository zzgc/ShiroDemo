package com.gc.common;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 2018/2/7 0007.
 */
public class Constant {
    public static final String ADD = "add";
    public static final String DELETE = "delete";
    public static final String UPDATE = "update";
    public static final String SELECT = "select";
    public static final String USER= "user";
    public static final String ROLE= "role";
    public static final String PERMISSION= "permission";
    public static final List<String> managerStr = Arrays.asList(USER,ROLE,PERMISSION);
    public static final List<String> permissionStr = Arrays.asList(ADD,DELETE,UPDATE,SELECT);
}
