package com.gc.service.impl;

import com.gc.dao.UserDao;
import com.gc.entity.Users;
import com.gc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Set;


/**
 * Created by Administrator on 2018/1/19 0019.
 */
@Transactional
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao ud;


    public Users login(String name, String pwd) {
        System.out.println(ud);
        return ud.login(name,pwd);
    }

    public List<Users> list() {
        return ud.list();
    }

    public Users get(Serializable id) {
        return ud.get(id);
    }

    public int update(Users u) {
        return ud.update(u);
    }

    public int delete(Serializable id) {
        return ud.delete(id);
    }

    public int add(Users r) {
        return ud.add(r);
    }

    public int editUserRole(Users users) {
        deleteUserRole(users.getId());
        int num = ud.addUserRole(users);
        return num;
    }

    public int updatePwd(int uid, String pwd) {
        return ud.updatePWD(uid,pwd);
    }

    public int deleteUserRole(Serializable id) {
        return ud.deleteUserRole(id);
    }

    public Set<String> getUserPermission(Serializable id) {
        return ud.getUserPermission(id);
    }

}
