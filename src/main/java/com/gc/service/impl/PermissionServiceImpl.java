package com.gc.service.impl;

import com.gc.dao.PermissionDao;
import com.gc.entity.Permission;
import com.gc.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/2/2.
 */
@Transactional
@Service
public class PermissionServiceImpl implements PermissionService{

    @Autowired
    PermissionDao pd;

    public List<Permission> list() {
        return pd.list();
    }

    public Permission get(Serializable id) {
        return pd.get(id);
    }

    public int update(Permission p) {
        return pd.update(p);

    }

    public int delete(Serializable id) {
        return pd.delete(id);
    }

    public int add(Permission p) {
        return pd.add(p);
    }
}
