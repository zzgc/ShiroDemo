package com.gc.service.impl;

import com.gc.dao.RoleDao;
import com.gc.entity.Role;
import com.gc.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/2/2.
 */
@Transactional
@Service
public class RoleServiceImpl implements RoleService{

    @Autowired
    RoleDao rd;

    public List<Role> list() {
        return rd.list();
    }

    public Role get(Serializable id) {
        return rd.get(id);
    }

    public int update(Role r) {
        return rd.update(r);
    }

    public int delete(Serializable id) {
        return rd.delete(id);
    }

    public int add(Role r) {
        return rd.add(r);
    }

    public int  editRolePermission(Role r){
        deleteRolePermission(r.getId());
        return rd.addRolePermission(r);
    }
    public int deleteRolePermission(Serializable id){
        return rd.deleteRolePermission(id);
    }
}
