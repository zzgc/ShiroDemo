package com.gc.service;

import com.gc.entity.Users;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2018/1/19 0019.
 */
public interface UserService {
    Users login(String name,String pwd);
    List<Users> list();
    Users get(Serializable id);
    int update(Users u);
    int delete(Serializable id);
    int add(Users u);
    int editUserRole(Users users);
    int updatePwd(int uid,String pwd);
    int deleteUserRole(Serializable id);
    Set<String> getUserPermission(Serializable id);
}
