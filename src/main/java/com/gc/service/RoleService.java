package com.gc.service;

import com.gc.entity.Permission;
import com.gc.entity.Role;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/1/19 0019.
 */
public interface RoleService {
    List<Role> list();
    Role get(Serializable id);
    int update(Role r);
    int delete(Serializable id);
    int add(Role r);
    int editRolePermission(Role r);
    int deleteRolePermission(Serializable id);
}
