package com.gc.service;

import com.gc.entity.Permission;
import com.gc.entity.Users;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/1/19 0019.
 */
public interface PermissionService {
    List<Permission> list();
    Permission get(Serializable id);
    int update(Permission p);
    int delete(Serializable id);
    int add(Permission p);
}
