package com.gc.dao;

import com.gc.entity.Users;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2018/1/19 0019.
 */
public interface UserDao {
    List<Users> list();
    Users get(Serializable id);
    int update(Users users);
    int delete(Serializable id);
    int add(Users users);
    Users login(@Param("username") String name, @Param("userpwd") String pwd);
    int addUserRole(Users users);
    int updatePWD(@Param("id") int uid, @Param("userpwd") String pwd);
    int deleteUserRole(Serializable uid);
    Set<String> getUserPermission(Serializable id);
}
