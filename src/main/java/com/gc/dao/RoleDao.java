package com.gc.dao;

import com.gc.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/2/1.
 */
public interface RoleDao {
    int addRolePermission(Role r);
    int deleteRolePermission(Serializable id);
    List<Role> list();
    Role get(Serializable id);
    int update(Role r);
    int delete(Serializable id);
    int add(Role r);
}
