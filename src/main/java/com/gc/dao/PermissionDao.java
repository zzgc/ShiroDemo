package com.gc.dao;

import com.gc.entity.Permission;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/2/1.
 */
public interface PermissionDao {
    List<Permission> list();
    Permission get(Serializable id);
    int update(Permission r);
    int delete(Serializable id);
    int add(Permission r);
}
