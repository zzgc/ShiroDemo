package com.gc.test;

import com.gc.entity.Permission;
import com.gc.entity.Role;
import com.gc.service.RoleService;
import com.gc.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

/**
 * Created by Administrator on 2018/2/2.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring.xml","classpath:spring-mvc.xml"})
public class JunitTest {

    @Autowired
    UserService us;

    @Autowired
    RoleService rs;

    @Test
    public void test(){
        Role role = new Role(1,"ceshi");
        role.setPermissions(Arrays.asList(new Permission(1,"p1"),new Permission(2,"p2")));
        rs.editRolePermission(role);
    }
}
