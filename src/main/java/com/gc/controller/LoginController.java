package com.gc.controller;

import com.gc.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * Created by Administrator on 2018/2/6 0006.
 */
@Controller
public class LoginController {
    @RequestMapping("/login")
    public String login(String name, String pwd, Map<String,Object> map){
        Subject currUser = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(name,pwd);
        String error = "登录成功！";
        try {
            currUser.login(token);
            map.put("error",error);
            return "redirect:/main";
        } catch (AuthenticationException e) {
            e.printStackTrace();
            error="登录失败！";
            map.put("error",error);
        }
        return "redirect:/error.jsp";
    }

    @Autowired
    UserService us;
    @RequestMapping("/main")
    public String main(){
        return "main";
    }

}
