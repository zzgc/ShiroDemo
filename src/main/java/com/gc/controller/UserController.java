package com.gc.controller;

import com.gc.entity.Users;
import com.gc.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * Created by Administrator on 2018/1/19 0019.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping
    public String usermanager(){
        return "userManager";
    }

    @Autowired
    UserService us;

    @RequestMapping("/list")
    public String list(Model model){
        model.addAttribute("list",us.list());
        return "userManager";
    }

    @RequestMapping("/edituserrole")
    public String editUserRole(Users users,String msg){
        int num = us.editUserRole(users);
        msg = "修改失败！";
        if(num>0){
           msg = "修改成功";
        }
        return "redirect:/user/list";
    }

    @RequestMapping("/update")
    public String editUser(Users users,String msg){
        int num = us.update(users);
        msg = "修改失败！";
        if(num>0){
            msg = "修改成功";
        }
        return "redirect:/user/list";
    }

    @RequestMapping("/delete")
    public String deleteUser(@RequestParam(required = true) int id, String msg){
        int num = us.delete(id);
        msg = "删除失败！";
        if(num>0){
            msg = "删除成功";
        }
        return "redirect:/user/list";
    }

    @RequestMapping("/add")
    public String addUser(Users users,String msg){
        int num = us.add(users);
        msg = "新增失败！";
        if(num>0){
            msg = "新增成功";
        }
        return "redirect:/user/list";
    }
    @RequestMapping("/updatepwd")
    public String upateUserPwd(String pwd, String msg, HttpSession session){
        Users user = (Users) session.getAttribute("user");
        int num = us.updatePwd(user.getId(),pwd);
        msg = "修改失败！";
        if(num>0){
            msg = "修改成功";
        }
        return "redirect:/user/list";
    }


}
