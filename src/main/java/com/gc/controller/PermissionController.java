package com.gc.controller;

import com.gc.entity.Permission;
import com.gc.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Administrator on 2018/2/2 0002.
 */
@Controller
@RequestMapping("/permission")
public class PermissionController {
    @Autowired
    PermissionService ps;
    String msg = "";

    @RequestMapping
    public String permissionManager(){
        return "permissionManager";
    }

    @RequestMapping("/add")
    public String add(Permission p, Model model){
        int num = ps.add(p);
        if(num > 0 ){
            msg = "新增成功！";
        }else{
            msg="新增失败！";
        }
        model.addAttribute("msg",msg);
        return "redirect:/permission/list";
    }
    @RequestMapping("/list")
    public String list(Model model){
        model.addAttribute("list",ps.list());
        return "/permissionManager";
    }

    @RequestMapping("/get/{id}")
    @ResponseBody
    public Permission get(@PathVariable("id") int id){
        Permission p = ps.get(id);
        return p;
    }
}
