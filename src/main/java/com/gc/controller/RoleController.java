package com.gc.controller;

import com.gc.entity.Role;
import com.gc.service.PermissionService;
import com.gc.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/2/5 0005.
 */
@Controller
@RequestMapping("/role")
public class RoleController {
    @RequestMapping
    public String roleManager(){
        return "roleManager";
    }
    @Autowired
    RoleService roleService;
    @Autowired
    PermissionService permissionService;

    @RequestMapping("/add")
    public String add(Role role){
        int num = roleService.add(role);
        if(num>0){
            System.out.println("新增成功！");
        }
        return "redirect:/role/list";
    }

    @RequestMapping("/list")
    public String list(Model model){
        List<Role> roles = roleService.list();
        model.addAttribute("roles",roles);
        return "roleManager";
    }

    @RequestMapping("/getpermission")
    @ResponseBody
    public Map<String,Object> getPermission(int id){
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("role",roleService.get(id));
        map.put("permissions",permissionService.list());
        return  map;
    }

    @RequestMapping("editpermissions")
    public String editPermissions(Role role){
        roleService.editRolePermission(role);
        return "redirect:/role/list";
    }



}
