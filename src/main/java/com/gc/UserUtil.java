package com.gc;

import com.gc.entity.Permission;
import com.gc.entity.Role;
import com.gc.entity.Users;

import java.beans.PersistenceDelegate;
import java.util.*;

/**
 * Created by Administrator on 2018/1/28.
 */
public class UserUtil {
    static List<Users> users = new ArrayList<Users>();
    static{
        List<Permission> list = new ArrayList<Permission>();
        Permission per1 = new Permission(1,"新增");
        Permission per2 = new Permission(2,"查询");
        Permission per3 = new Permission(3,"修改");
        Permission per4 = new Permission(4,"删除");
        list.add(per1);
        list.add(per2);
        list.add(per3);
        list.add(per4);

        Role role = new Role(1,"管理员");
        role.setPermissions(list);

        Role role1 = new Role(2,"普通用户");
        role1.setPermissions(Arrays.asList(per1,per2));

        Role role2 = new Role(3,"次级管理员");
        role2.setPermissions(Arrays.asList(per1,per2,per3));

        Users user = new Users(1,"张三","123");
        user.setRoles(Arrays.asList(role,role1,role2));

        Users user1 = new Users(2,"李四","456");
        user1.setRoles(Arrays.asList(role1));

        Users user2 = new Users(3,"王武","456");
        user1.setRoles(Arrays.asList(role2));
        users.add(user);
        users.add(user1);
        users.add(user2);
    }
    public static Users CheckName(String name){
        for(Users u : users){
            if(u.getUsername().equals(name)){
                return u;
            }
        }
        return null;
    }

    public static Set<String> getRoleByName(String name){
        Set<String> roles = new HashSet<String>();
        for(Users u : users){
            if(u.getUsername().equals(name)){
                for(Role r : u.getRoles()){
                    roles.add(r.getRolename());
                }
            }
        }
        return roles;
    }
    public static Set<String> getPermissionByName(String name){
        HashSet<String>  permissions = new HashSet<String>();
        for(Users u : users){
            if(u.getUsername().equals(name)){
                for(Role r : u.getRoles()){
                   for(Permission permission : r.getPermissions()){
                       permissions.add(permission.getPername());
                   }
                }
            }
        }
        return permissions;
    }


}
