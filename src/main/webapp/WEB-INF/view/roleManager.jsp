<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/2/1
  Time: 13:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>Title</title>
    <script src="/static/js/jquery-3.0.0.min.js"></script>
</head>
<body>
<shiro:hasPermission name="role:add">
    <button>新增</button>
</shiro:hasPermission>
<shiro:hasPermission name="role:update">
    <button>修改</button>
</shiro:hasPermission>
<shiro:hasPermission name="role:select">
    <button>查询</button>
</shiro:hasPermission>
<shiro:hasPermission name="role:delete">
    <button>删除</button>
</shiro:hasPermission>
<shiro:hasPermission name="role:editpermission">
    <button>分配权限</button>
</shiro:hasPermission>
<table>
    <tr><td>序号</td><td>角色名称</td><td>权限信息</td><td>操作</td></tr>
    <c:forEach items="${roles}" var="r" varStatus="i">
        <tr><td>${i.count}</td><td>${r.rolename}</td><td>
            <c:forEach items="${r.permissions}" var="p">
                ${p.pername},
            </c:forEach>
        </td><td><a>分配权限</a><a>修改</a>&nbsp;<a>删除</a></td></tr>
    </c:forEach>
</table>
<hr>
<fieldset>
    <form action="/role/add" method="post">
   <legend>新增角色</legend>
    <input name="rolename"/>
    <input type="submit" value="新增"/>
    </form>
</fieldset>
<hr>
<fieldset>
    <form action="/role/editpermissions" method="post" onsubmit="return checksubmit()">
    <legend>分配权限</legend>
    <select onchange="getrolepermission(this)" name="id">
        <option value="">选择角色</option>
        <c:forEach items="${roles}" var="r">
            <option value="${r.id}">${r.rolename}</option>
        </c:forEach>
    </select>
    <div>
        权限列表：
        <ul id="permissionul">
          <%--  <li><input type='checkbox' checked/>测试</li>--%>
        </ul>
        <button type="submit" >确定</button>
    </div>
    </form>
</fieldset>
<script>
    function getrolepermission(sel) {
        var val = sel.value;
        if(val!=""){
            $.post("/role/getpermission","id="+val,function (r) {
                var ps = r.permissions;
                var role = r.role;
                $("#permissionul li").remove();
                for(var i=0;i<ps.length;i++){
                    var flag = false;
                    for(var j=0;j<role.permissions.length;j++){
                        if(role.permissions[j].id==ps[i].id){
                            flag = true;
                            break;
                        }
                    }
                    var checkbox = "<input type='checkbox' name='permissions["+i+"].id' value='"+ps[i].id+"'";
                    if(flag){
                        checkbox+=" checked ";
                    }
                    checkbox+="/>"
                    $("#permissionul").append("<li>"+checkbox+ps[i].pername+"</li>");
                }
            });
        }
    }

    function checksubmit() {
       var cks = $("#permissionul :checkbox:checked");
        if(!cks || cks.length<1){
            alert("请选择权限！");
            return false;
        }
    }

</script>
</body>
</html>
