<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="UTF-8">
    <title>权限管理系统</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/media/css/jquery.dataTables.min.css" rel="stylesheet">
    <script type="text/javascript"
            src="<%=basePath%>/static/js/jquery-3.0.0.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <link href="<%=basePath%>/static/css/toastr.css" rel="stylesheet"/>
    <script src="<%=basePath%>/static/js/toastr.js"></script>
    <script src="/static/media/js/jquery.dataTables.min.js"></script>

</head>

<body>
<script>
    var msg = '${msg}';
    var parammsg = '${parame.msg}';
    if (msg != '') {
        toastr.info(msg);
    } else if (parammsg != '') {
        toastr.info(parammsg);
    }
</script>
<%@include file="/common/nav.html" %>
<div class="container-fluid">
    <div class="row">
        <%@include file="/common/left.html" %>
        <div class="col-sm-9">
            <ol class="breadcrumb">
                <li>主页</li>
                <li class="active">权限管理</li>
            </ol>
            <div class="table-responsive">
                <shiro:hasPermission name="permission:select">
                <button type="button" class="btn btn-primary" onclick="javascript:location.href='/permission/list'">查询</button>
                </shiro:hasPermission>
                <shiro:hasPermission name="permission:add">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addPermissionModal">新增</button>
                </shiro:hasPermission>
                <shiro:hasPermission name="permission:update">
                <button type="button" class="btn btn-warning">修改</button>
                </shiro:hasPermission>
                <shiro:hasPermission name="permission:delete">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete">删除</button>
                </shiro:hasPermission>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>权限名称</th>
                        <th>别名</th>
                        <th>模块</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="p" varStatus="i">
                    <tr>
                        <td>${i.count}</td>
                        <td>${p.pername}</td>
                        <td>${p.identification}</td>
                        <td>${p.mname}</td>
                        <td>
                            <button type="button" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                修改
                            </button>
                            <button type="button" class="btn btn-default btn-sm"
                                    data-id="1" data-toggle="modal"
                                    data-target="#confirm-delete">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                删除
                            </button>
                        </td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <nav aria-label="Page navigation" style="text-align: center">
                    <ul class="pagination">
                        <li><a aria-label="Previous"> <span aria-hidden="true">上一页</span>
                        </a></li>
                        <li><a  aria-label="Next"> <span aria-hidden="true">下一页</span>
                        </a></li>
                    </ul>
                </nav>
            </div>
            <hr>
            <table id="dt">
                <thead>
                <tr>
                    <th>序号</th>
                    <th>权限名称</th>
                    <th>别名</th>
                    <th>模块</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>序号</td>
                    <td>权限名称</td>
                    <td>别名</td>
                    <td>模块</td>
                    <td>操作</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- 确认删除的模态窗口 -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">请确认</div>
            <div class="modal-body">确认删除该记录吗？</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <a class="btn btn-danger btn-ok">删除记录</a>
            </div>
        </div>
    </div>
</div>
<!-- 新增权限的模态窗口 -->
<div class="modal fade" id="addPermissionModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                新增权限
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="addpermissionform" action="" method="post">
                    <div class="form-group">
                        <label for="modul" class="col-sm-2 control-label">模块</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="" id="modul" style="width: 300px;">
                                <option value="1">用户管理</option>
                                <option value="2">角色管理</option>
                                <option value="3">权限管理</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pname" class="col-sm-2 control-label">权限名称</label>
                        <div class="col-sm-10">
                            <input type="text" name="" class="form-control" id="pname"  placeholder="权限名称" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alias" class="col-sm-2 control-label">权限别名</label>
                        <div class="col-sm-10">
                            <input type="text" name="" class="form-control" id="alias"  placeholder="权限别名" >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info">新增</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#dt').DataTable();
    });
    //给删除按钮添加事件，当弹出确认窗口时，给确认窗口内的.btn-ok（即确认提交按钮）的href属性赋值
    //主要是为了实现传递不同的ID，因为提示窗口是固定的，但每个删除按钮所传的ID是不同的
    $('#confirm-delete').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        $(this).find('.btn-ok').attr('href', "delete?id=" + id);
    });
    //给弹出的显示详情窗口添加事件，在窗口显示时，异步请求服务端，获取当前要显示的数据，
    //并通过JS将数据显示到当前的窗口中。
    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // 触发事件的按钮
        var whatever = button.data('whatever'); // 解析出data-whatever内容
        var modal = $(this);
        $.post("getvote.action", "id=" + whatever, function (r) {
            modal.find('.modal-title').text(r.vtitle);
            modal.find("#fat-btn").text(r.voteType.vtName);
            var ops = r.voteOptions;
            var modalbody = modal.find('.modal-body');
            modalbody.children().remove();
            for (var i = 0; i < ops.length; i++) {
                var op = ops[i];
                modalbody.append("<div class='op well well-sm'>"
                        + op.voText + "</div>");
            }
        });
        //modal.find('.modal-body input').val(recipient)
    })
</script>
</body>

</html>