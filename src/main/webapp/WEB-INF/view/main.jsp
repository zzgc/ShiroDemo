<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="UTF-8">
    <title>权限管理系统</title>
    <link href="<%=basePath%>/static/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript"
            src="<%=basePath%>/static/js/jquery-3.0.0.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>/static/js/bootstrap.min.js"></script>
    <link href="<%=basePath%>/static/css/toastr.css" rel="stylesheet"/>
    <script src="<%=basePath%>/static/js/toastr.js"></script>
</head>

<body>
<script>
    var msg = '${msg}';
    var parammsg = '${parame.msg}';
    if (msg != '') {
        toastr.info(msg);
    } else if (parammsg != '') {
        toastr.info(parammsg);
    }
</script>
<%@include file="/common/nav.html" %>
<div class="container-fluid">
    <div class="row">
        <%@include file="/common/left.html" %>
        <div class="col-sm-9">
            <ol class="breadcrumb">
                <li class="active">主页</li>
            </ol>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>分类</th>
                        <th>投票名</th>
                        <th>参与人数</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>
                            <button type="button" class="btn btn-default btn-sm"
                                    data-toggle="modal" data-target="#exampleModal"
                                    data-whatever="1">
                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                详情
                            </button>
                            <button type="button" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                修改
                            </button>
                            <button type="button" class="btn btn-default btn-sm"
                                    data-id="1" data-toggle="modal"
                                    data-target="#confirm-delete">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                删除
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <nav aria-label="Page navigation" style="text-align: center">
                    <ul class="pagination">
                        <li><a aria-label="Previous"> <span aria-hidden="true">上一页</span>
                        </a></li>
                        <li><a  aria-label="Next"> <span aria-hidden="true">下一页</span>
                        </a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- 确认删除的模态窗口 -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">请确认</div>
            <div class="modal-body">确认删除该记录吗？</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <a class="btn btn-danger btn-ok">删除记录</a>
            </div>
        </div>
    </div>
</div>
<!-- 投票详情模态窗口 -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">投票标题</h4>
                <button id="fat-btn" class="btn btn-primary" type="button">投票类型
                </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    //给删除按钮添加事件，当弹出确认窗口时，给确认窗口内的.btn-ok（即确认提交按钮）的href属性赋值
    //主要是为了实现传递不同的ID，因为提示窗口是固定的，但每个删除按钮所传的ID是不同的
    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', "deletevote.action?id=" + $(e.relatedTarget).data('id'));
    });
    //给弹出的显示详情窗口添加事件，在窗口显示时，异步请求服务端，获取当前要显示的数据，
    //并通过JS将数据显示到当前的窗口中。
    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // 触发事件的按钮
        var whatever = button.data('whatever'); // 解析出data-whatever内容
        var modal = $(this);
        $.post("getvote.action", "id=" + whatever, function (r) {
            modal.find('.modal-title').text(r.vtitle);
            modal.find("#fat-btn").text(r.voteType.vtName);
            var ops = r.voteOptions;
            var modalbody = modal.find('.modal-body');
            modalbody.children().remove();
            for (var i = 0; i < ops.length; i++) {
                var op = ops[i];
                modalbody.append("<div class='op well well-sm'>"
                        + op.voText + "</div>");
            }
        });
        //modal.find('.modal-body input').val(recipient)
    })
</script>
</body>

</html>