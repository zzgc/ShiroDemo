<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/2/1
  Time: 13:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<shiro:hasPermission name="user:add">
    <button>新增</button>
</shiro:hasPermission>
<shiro:hasPermission name="user:update">
    <button>修改</button>
</shiro:hasPermission>
<shiro:hasPermission name="user:select">
    <button>查询</button>
</shiro:hasPermission>
<shiro:hasPermission name="user:delete">
    <button>删除</button>
</shiro:hasPermission>
<shiro:hasPermission name="user:editrole">
    <button>分配角色</button>
</shiro:hasPermission>
<table>
    <tr><td>序号</td><td>用户名</td><td>角色</td><td>操作</td></tr>
    <c:forEach items="${list}" var="u" varStatus="i">
        <tr><td>${i.count}</td><td>${u.username}</td><td>
            <c:forEach items="${u.roles}" var="r">
                ${r.rolename},
            </c:forEach>
        </td><td><a>分配角色</a>&nbsp;<a>修改</a>&nbsp;<a>删除</a></td></tr>
    </c:forEach>
</table>

</body>
</html>
