/*
Navicat MySQL Data Transfer

Source Server         : HXZY
Source Server Version : 50637
Source Host           : localhost:3306
Source Database       : shirodb

Target Server Type    : MYSQL
Target Server Version : 50637
File Encoding         : 65001

Date: 2018-09-18 16:29:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pername` varchar(255) DEFAULT NULL,
  `moduleid` int(11) DEFAULT NULL,
  `identification` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('1', '新增', '1', 'user:add');
INSERT INTO `permission` VALUES ('2', '修改', '1', 'user:update');
INSERT INTO `permission` VALUES ('3', '查询', '1', 'user:select');
INSERT INTO `permission` VALUES ('4', '删除', '1', 'user:delete');
INSERT INTO `permission` VALUES ('5', '新增', '2', 'role:add');
INSERT INTO `permission` VALUES ('6', '修改', '2', 'role:update');
INSERT INTO `permission` VALUES ('7', '查询', '2', 'role:select');
INSERT INTO `permission` VALUES ('8', '删除', '2', 'role:delete');
INSERT INTO `permission` VALUES ('9', '分配权限', '2', 'role:editpermission');
INSERT INTO `permission` VALUES ('10', '新增', '3', 'permission:add');
INSERT INTO `permission` VALUES ('11', '修改', '3', 'permission:update');
INSERT INTO `permission` VALUES ('12', '查询', '3', 'permission:select');
INSERT INTO `permission` VALUES ('13', '删除', '3', 'permission:delete');
INSERT INTO `permission` VALUES ('14', '分配角色', '1', 'user:editrole');

-- ----------------------------
-- Table structure for permissionmodule
-- ----------------------------
DROP TABLE IF EXISTS `permissionmodule`;
CREATE TABLE `permissionmodule` (
  `id` int(11) NOT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permissionmodule
-- ----------------------------
INSERT INTO `permissionmodule` VALUES ('1', '用户管理', 'user');
INSERT INTO `permissionmodule` VALUES ('2', '角色管理', 'role');
INSERT INTO `permissionmodule` VALUES ('3', '权限管理', 'permission');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rolename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'sdf');
INSERT INTO `role` VALUES ('2', 'cece');
INSERT INTO `role` VALUES ('3', '管理员');
INSERT INTO `role` VALUES ('4', '普通用户');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `rid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES ('1', '1');
INSERT INTO `role_permission` VALUES ('1', '2');
INSERT INTO `role_permission` VALUES ('3', '7');
INSERT INTO `role_permission` VALUES ('3', '8');
INSERT INTO `role_permission` VALUES ('4', '1');
INSERT INTO `role_permission` VALUES ('4', '2');
INSERT INTO `role_permission` VALUES ('4', '3');
INSERT INTO `role_permission` VALUES ('4', '4');
INSERT INTO `role_permission` VALUES ('3', '11');
INSERT INTO `role_permission` VALUES ('3', '10');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `userpwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'zs', '123');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `userid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', '4');
INSERT INTO `user_role` VALUES ('1', '3');
