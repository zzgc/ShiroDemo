package threaddemo;

/**
 * Created by Administrator on 2018/2/22 0022.
 */
public class AccessToMoney implements Runnable {

    static Integer money = 1000;
    public String name="";
    public AccessToMoney(String name){
        this.name = name;
    }
    public void run() {
        synchronized(money){
            while (money>0){
                money=money-100;
                System.out.println("线程"+Thread.currentThread().getName()+"正在运行,"+name+"取钱100元，当前余额为："+money);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
