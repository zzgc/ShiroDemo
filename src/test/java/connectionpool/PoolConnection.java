package connectionpool;

import java.sql.Connection;

/**
 * 连接池
 * Created by Administrator on 2018/2/23 0023.
 */
public class PoolConnection {

    private Connection connection;
    private boolean isUse = false;

    public PoolConnection(Connection connection) {
        this.connection = connection;
    }


    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public boolean isUse() {
        return isUse;
    }

    public void setUse(boolean use) {
        isUse = use;
    }
}
