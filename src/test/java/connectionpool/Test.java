package connectionpool;

import java.sql.Connection;
import java.util.Random;

/**
 * Created by Administrator on 2018/2/23 0023.
 */
public class Test {
    public static void main(String[] args) {

        final ConnectionPool pool = ConnectionPool.getPool();
        for(int i = 0;i<15;i++){
            new Thread(new Runnable() {
                public void run() {
                   Connection connection = pool.getConnection();
                    System.out.println(Thread.currentThread().getName()+"获取一个连接"+connection+",当前连接池剩余连接为："+pool.getConnectionCount());
                    int sec = (int) (Math.random()*10000);
                    try {
                        Thread.sleep(sec);
                        pool.returnConnection(connection);
                        System.out.println(Thread.currentThread().getName()+"连接使用完毕，放回连接池,当前连接池剩余连接为："+pool.getConnectionCount());

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }


    }
}
