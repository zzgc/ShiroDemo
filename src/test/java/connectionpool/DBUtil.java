package connectionpool;

import sun.applet.Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Administrator on 2018/2/23 0023.
 */
public class DBUtil {
    private static String url = "jdbc:mysql://localhost:3306/shirodb?useUnicode=true&characterEncoding=utf8";
    private static String drivername = "com.mysql.jdbc.Driver";
    private static String username = "root";
    private static String userpwd = "hxzy123";

    public static Connection getConnection(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url,username,userpwd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

}
